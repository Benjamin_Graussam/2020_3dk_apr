public class GuestDataNotValidExeption extends Throwable {

    private String message;

    public GuestDataNotValidExeption(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
