// AVK3: 12 Punkte
// Sehr gute Arbeit
// ... beachte aber meine Anmerkungen für zuknüftige Abgaben

public class GuestRigistationValidator {

    // die Methode sollte validate() heißen
    public void Validator(String firstnametxt,String lastnametxt,String emailtxt,
                          String tabletxt,String numbertxt) throws GuestDataNotValidExeption {

        String nothing = "";
        try {
            isFirstNameEmpty(firstnametxt);
        }catch (IllegalArgumentException e) {
            nothing += e.getMessage() + "\n";
        }
        try {
            isLastNameEmpty(lastnametxt);
        }catch (IllegalArgumentException e) {
            nothing += e.getMessage() + "\n";
        }
        try {
            isItMax(firstnametxt,lastnametxt);
        }catch (IllegalArgumentException e) {
            nothing += e.getMessage() + "\n";
        }
        try {
            EmailAndNumberquery(emailtxt,numbertxt);
        }catch (IllegalArgumentException e) {
            nothing += e.getMessage() + "\n";
        }
        try {
            TableNumber(tabletxt);
        }catch (IllegalArgumentException e) {
            nothing += e.getMessage() + "\n";
        }
        if (!nothing.isEmpty()) {
            throw new GuestDataNotValidExeption(nothing);
        }
    }
    private boolean isItEmpty(String placeholder){
        if(placeholder == null || placeholder.isEmpty()) {
            return true;
        }else return false;}

    private boolean checkLength(String placeholder, Integer value) {
        if (placeholder.length() > value) {
            return true;
        }else{
            return false;
        }
    }
    private void isFirstNameEmpty(String lastnametxt) throws GuestDataNotValidExeption{
        if (isItEmpty(lastnametxt) || checkLength(lastnametxt, 15)){
            throw new IllegalArgumentException("First name must not be empty or longer than 15 characters.");
        }
    }
    private void isLastNameEmpty(String lastnametxt) throws GuestDataNotValidExeption{
        if (isItEmpty(lastnametxt) || checkLength(lastnametxt, 20)){
            throw new IllegalArgumentException("Last name must not be empty or longer than 20 characters.");
        }
    }
    private void isItMax(String firstnametxt, String lastnametxt) throws GuestDataNotValidExeption{
        if (firstnametxt.equals("Max") && lastnametxt.equals("Mustermann")) {
            throw new IllegalArgumentException("Max Mustermann is not allowed!");
        }
    }

    // Methodennamen immer klein
    private void EmailAndNumberquery(String emailtxt, String numbertxt) throws GuestDataNotValidExeption{
        if (isItEmpty(emailtxt) && isItEmpty(numbertxt)) {
            throw new IllegalArgumentException("Email or phone number must be provided.");
        }
    }
    private void TableNumber(String tabletxt) throws GuestDataNotValidExeption{
        if (tabletxt.isEmpty()) {
            throw new IllegalArgumentException("Table number dont be empty.");
        }
        // zwischen 1 und 100 war gefordert
        if (Integer.parseInt(tabletxt) < 0 && Integer.parseInt(tabletxt) > 100){
            throw new IllegalArgumentException("Table number must be between 1 and 100.");
        }
    }
}