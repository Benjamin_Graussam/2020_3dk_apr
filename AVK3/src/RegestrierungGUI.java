import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;



public class RegestrierungGUI extends JFrame {

    public RegestrierungGUI(String title) {
        setPreferredSize(new Dimension(640,275));
        setLayout(null);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);

        JLabel headline = new JLabel("Gästeregistrierung");
        headline.setBounds(230,10,250,30);
        headline.setFont(new Font("Arial",Font.BOLD,22));

        JLabel firstName = new JLabel("Vorname:");
        firstName.setBounds(10,60,100,20);

        JLabel lastName = new JLabel("Nachname:");
        lastName.setBounds(10,85,100,20);

        JLabel street = new JLabel("Straße:");
        street.setBounds(10,110,100,20);

        JLabel plz = new JLabel("PLZ:");
        plz.setBounds(10,135,100,20);

        JLabel place = new JLabel("Ort:");
        place.setBounds(10,160,100,20);

        TextField firstnametxt = new TextField();
        firstnametxt.setBounds(140,60,140,20);

        TextField lastnametxt = new TextField();
        lastnametxt.setBounds(140,85,140,20);

        TextField streettxt = new TextField();
        streettxt.setBounds(140,110,140,20);

        TextField plztxt = new TextField();
        plztxt.setBounds(140, 135,50,20);

        TextField placetxt = new TextField();
        placetxt.setBounds(140,160,140,20);

        JLabel telefonnumber = new JLabel("Telefonnummer:");
        telefonnumber.setBounds(360,60,100,20);

        JLabel eMail = new JLabel("E-Mail:");
        eMail.setBounds(360,85,100,20);

        JLabel tableNumber = new JLabel("Tisch-Nr:");
        tableNumber.setBounds(360,110,100,20);

        TextField numbertxt = new TextField();
        numbertxt.setBounds(480,60,140,20);

        TextField emailtxt = new TextField();
        emailtxt.setBounds(480,85,140,20);

        TextField tabletxt = new TextField();
        tabletxt.setBounds(480,110,140,20);


        JButton save = new JButton("Speichern");
        save.setBounds(200,200,100,25);

        JButton reset = new JButton("Zurücksetzen");
        reset.setBounds(310,200,120,25);

        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GuestRigistationValidator guestRegistrationValidator = new GuestRigistationValidator();
                try {
                    guestRegistrationValidator.Validator(firstnametxt.getText(),lastnametxt.getText(), emailtxt.getText(), tabletxt.getText(),numbertxt.getText());
                } catch (GuestDataNotValidExeption guestDataNotValidException) {
                    System.err.println(guestDataNotValidException.getMessage());
                }
            }

        });


        add(headline);
        add(firstName);
        add(lastName);
        add(street);
        add(plz);
        add(place);
        add(telefonnumber);
        add(eMail);
        add(tableNumber);

        add(firstnametxt);
        add(lastnametxt);
        add(streettxt);
        add(plztxt);
        add(numbertxt);
        add(emailtxt);
        add(tabletxt);

        add(save);
        add(reset);


        pack();


    }}


