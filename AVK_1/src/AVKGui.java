import javax.swing.*;
import java.awt.*;

public class AVKGui extends JFrame {
    public AVKGui(String title) {
        super(title);
        setLayout(new FlowLayout());



        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        Upperpart up = new Upperpart();
        up.setPreferredSize(new Dimension(620,150));
        add(up);

        Lowerpart low = new Lowerpart();
        low.setPreferredSize(new Dimension(620,50));
        add(low);

        pack();
        setSize(620,250);


    }
}

