import javax.swing.*;
import java.awt.*;

public class Upperpart extends JPanel { // schlechter Klassenname
    public Upperpart() {
        setLayout(null); // hier wäre noch LayoutManagement sinnvoll gewesen: -1

        //Jlabels

        JLabel label = new JLabel("Gästeregestrierung");
        label.setFont(new Font("Calibri", Font.BOLD, 20));
        label.setBounds(240, 1, 200, 30);
        add(label);

        JLabel vn = new JLabel("Vorname: ");
        vn.setBounds(10,11,100,100);
        add(vn);

        JLabel nn = new JLabel("Nachname: ");
        nn.setBounds(10,31,100,100);
        add(nn);

        JLabel street = new JLabel("Straße");
        street.setBounds(10,51,100,100);
        add(street);

        JLabel PLZ = new JLabel("PLZ");
        PLZ.setBounds(10,71,100,100);
        add(PLZ);

        JLabel Ort = new JLabel("Ort");
        Ort.setBounds(10,91,100,100);
        add(Ort);

        //text

        JTextField text1 = new JTextField();
        text1.setBounds(110,55,100,16);
        add(text1);

        JTextField text2 = new JTextField();
        text2.setBounds(110,75,100,16);
        add(text2);

        JTextField text3 = new JTextField();
        text3.setBounds(110,95,100,16);
        add(text3);

        JTextField text4 = new JTextField();
        text4.setBounds(110,115,40,16);
        add(text4);

        JTextField text5 = new JTextField();
        text5.setBounds(110,135,100,16);
        add(text5);



        //right site

        JLabel tel = new JLabel("Telefonnummer: ");
        tel.setBounds(350,11,130,100);
        add(tel);

        JLabel mail = new JLabel("E-Mail: ");
        mail.setBounds(350,31,100,100);
        add(mail);

        JLabel Tisch = new JLabel("Tisch.NR: ");
        Tisch.setBounds(350,51,100,100);
        add(Tisch);

        //textfield

        JTextField text6 = new JTextField();
        text6.setBounds(500,55,100,16);
        add(text6);

        JTextField text7 = new JTextField();
        text7.setBounds(500,75,100,16);
        add(text7);

        JTextField text8 = new JTextField();
        text8.setBounds(500,95,100,16);
        add(text8);


    }
}
