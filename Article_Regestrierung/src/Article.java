import java.io.FileWriter;
import java.io.IOException;

public class Article {

    private int id;
    private String description;
    private double price;
    private int stock;

    public Article(int articlenum, String articlename, double articleprice, int articleamount) {
        this.id = articlenum;
        this.description = articlename;
        this.price = articleprice;
        this.stock = articleamount;
    }

    public void saveToFile() throws IOException {
        FileWriter writer = new FileWriter("article.txt");
        writer.write(this.toString());
        writer.close();
        System.out.println("Succesfully wrote to the file.");
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Article{");
        sb.append("id='").append(id).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", price='").append(price).append('\'');
        sb.append(", stock='").append(stock).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }
}
