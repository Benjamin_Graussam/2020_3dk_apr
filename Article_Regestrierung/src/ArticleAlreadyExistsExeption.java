public class ArticleAlreadyExistsExeption extends Exception{

    public ArticleAlreadyExistsExeption(int articleid){
        super("Article with id " + articleid + " already exists");
    }

}
