import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDataAccessObject {
    private final String user;
    private final String password;
    private final String databaseurl;

    public ArticleDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.databaseurl = "jdbc:mariadb://"+ host +":"+ port +"/"+database;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver");
        }
    }

    private Connection connect(){
        try {
            return DriverManager.getConnection(this.databaseurl,this.user,this.password);
        } catch (SQLException throwables){
           throw new RuntimeException();
        }
    }

    private void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.out.println("Closed connection");
        }
    }

    public void insert(Article article){
        Connection connection = connect();
        String sql = "insert into artikel(Artikelname,Verkaufspreis,Bestand) VALUES ('" + article.getDescription() + "','" + article.getPrice() + "','" + article.getStock() + "')";
        Statement insert = null;
        try {
            insert = connection.createStatement();
            insert.execute(sql);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statment", throwables);
        } finally {
            close(connection);
        }
        System.out.println("Insert Execute successfully");
    }
    public Article read(int id){
        Connection connection = connect();
        String sql = "Select * from artikel where Artikelnummer = " + id;
        Statement select = null;
        try {
            select = connection.createStatement();
            ResultSet resultSSet = select.executeQuery(sql);
            if (resultSSet.next()) {
                return new Article(
                        resultSSet.getInt(1),
                        resultSSet.getString(2),
                        resultSSet.getDouble(3),
                        resultSSet.getInt(4)
                );
            }
        } catch (SQLException throwables){
            System.err.println("Coule not run every!");
            throwables.printStackTrace();
        } finally {
            close(connection);
        }
        return null;
    }
    public void update(Article article){
        Connection connection = connect();
        String sql = "update artikel set Artikelname " + article.getDescription() + ", " + article.getPrice() +", " + article.getStock();
        Statement update = null;
        try{
            update = connection.createStatement();
            update.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            close(connection);
        }
    }
    public List<Article> findBy(String description){
        Connection connection = connect();
        String sql = "select * from artikel where lower(Artikelname) like '%"+description+"%'";
        Statement findBy = null;

        ArrayList<Article> article = new ArrayList<>();

        try {
            findBy = connection.createStatement();
            ResultSet resultSet = findBy.executeQuery(sql);
            while(resultSet.next()){
                article.add(new Article(
                        resultSet.getInt(1),resultSet.getString(2),resultSet.getDouble(3),resultSet.getInt(4)));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(connection);
        }
        return article;
    }


    public List<Article> readAllMessages(){
        Connection connection = connect();
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = "select * from artikel";
            ResultSet resultSet;
            resultSet = statement.executeQuery(sql);
            ArrayList<Article> article = new ArrayList<>();
            while(resultSet.next()){
                article.add(new Article(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getDouble(3),
                        resultSet.getInt(4)));
            }
            close(connection);
            return article;
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        }
    }
    public void delete(int id){
        Connection connection = connect();

        Statement statement = null;
        String sql = "DELETE FROM artikel WHERE Artikelnummer = " + id;

        try{
            statement = connection.createStatement();
            statement.executeQuery(sql);
            System.out.println("Deleted");

        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
            System.out.println("Closed Succesfully");
        }
    }

}


