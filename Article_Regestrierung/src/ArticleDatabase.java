import java.util.HashMap;

public class ArticleDatabase {

    private HashMap<Integer, Article> database;

    public ArticleDatabase() {
        this.database = new HashMap<>();
    }

    public void insert(Article article) throws ArticleAlreadyExistsExeption {
        if (database.containsKey(article.getId())){
            throw new ArticleAlreadyExistsExeption(article.getId());
        }
        this.database.put(article.getId(),article);
    }

    public void delete(int articleid) {
        if (database.containsKey(articleid)){
            this.database.remove(articleid);
        } else {
            throw new ArticleDoesNotExistExeption(articleid);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArticleDatabase{");
        sb.append("database=").append(database);
        sb.append('}');
        return sb.toString();
    }
}
