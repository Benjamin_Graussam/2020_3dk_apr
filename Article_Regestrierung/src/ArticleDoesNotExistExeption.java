public class ArticleDoesNotExistExeption extends RuntimeException{

    public ArticleDoesNotExistExeption(int articleid) {
        super("Article with id " + articleid + " does not exist");
    }
}
