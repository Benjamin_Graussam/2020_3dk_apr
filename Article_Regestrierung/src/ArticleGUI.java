import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ArticleGUI extends JPanel {

    private ArticleDataAccessObject dao;

    private final JTextField txtarticlenum = new JTextField();
    private final JTextField txtarticlename = new JTextField();
    private final JTextField txtarticleprice = new JTextField();
    private final JTextField txtarticleamount = new JTextField();

    public ArticleGUI(ArticleDataAccessObject dao, ArticleTableModel tableModel) {
        setPreferredSize(new Dimension(450,220));
        setLayout(null);
        //setBorder(new LineBorder(Color.BLUE,1));

        this.dao = dao;


        /*-- Die Labels --*/
        JLabel articlelbl = new JLabel("Artikel");
        JLabel articlenum = new JLabel("Artikelnummer:");
        JLabel articlename = new JLabel("Artikelname:");
        JLabel articleprice = new JLabel("Artikelpreis:");
        JLabel articleamount = new JLabel("Bestand:");

        articlelbl.setFont(new Font("Segoe UI",Font.BOLD,18));

        articlelbl.setBounds(25,25,75,25);
        articlenum.setBounds(25,75,125,25);
        articlename.setBounds(25,110,125,25);
        articleprice.setBounds(25,145,125,25);
        articleamount.setBounds(25,180,125,25);

        add(articlelbl);
        add(articlenum);
        add(articlename);
        add(articleprice);
        add(articleamount);

        /*-- Die TextFields --*/
        txtarticlenum.setBounds(150,75,100,25);
        txtarticlenum.setText("0");
        txtarticlename.setBounds(150,110,100,25);
        txtarticleprice.setBounds(150,145,100,25);
        txtarticleamount.setBounds(150,180,100,25);

        // Textfield nicht editier bar
        txtarticlenum.setEditable(false);



        add(txtarticlenum);
        add(txtarticlename);
        add(txtarticleprice);
        add(txtarticleamount);

        /*-- Die Buttons --*/
        JButton neu = new JButton("Neu");
        neu.setBounds(300,25,100,25);
        neu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetFields();
            }
        });

        JButton create = new JButton("Anlegen");
        create.setBounds(300,75,100,25);

        JButton change = new JButton("Ändern");
        change.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
             int id = Integer.parseInt(txtarticlenum.getText());
             String articleDes = checkEmpty(txtarticlename.getText());
             articleDes = checkMaxLength(articleDes,100);
             double articlePrice = checkDouble(txtarticleprice.getText());
             int stock = checkInteger(txtarticleamount.getText());
             Article a = new Article(id, articleDes, articlePrice, stock);
             dao.update(a);
             List<Article> articles = dao.readAllMessages();
             articles.forEach(System.out::println);
             tableModel.initData(articles);

            }
        });
        change.setBounds(300,110,100,25);

        JButton delete = new JButton("Löschen");
        delete.setBounds(300,145,100,25);
        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dao.delete(Integer.parseInt(txtarticlenum.getText()));
                List<Article> articles = dao.readAllMessages();
                articles.forEach(System.out::println);
                resetFields();
            }
        });

        JButton search = new JButton("Suchen");
        search.setBounds(300,180,100,25);
        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String desc = txtarticlename.getText();
                dao.findBy(desc);
                List<Article> foundArticles = dao.findBy(desc);
                tableModel.initData(foundArticles);
            }
        });


        create.addActionListener(e -> {
            Article article = new Article(checkInteger(checkEmpty(txtarticlenum.getText())),checkMaxLength(checkEmpty(txtarticlename.getText()),15),checkDouble(checkEmpty(txtarticleprice.getText())),checkInteger(checkEmpty(txtarticleamount.getText())));

            dao.insert(article);

            List<Article> articles = dao.readAllMessages();
            articles.forEach(System.out::println);
        });

        add(neu);
        add(create);
        add(change);
        add(delete);
        add(search);
    }
    public void resetFields(){
        this.txtarticlename.setText("");
        this.txtarticleprice.setText("");
        this.txtarticlenum.setText("");
        this.txtarticleamount.setText("");
    }

    private String checkEmpty(String value){
        if (value == null || value.isEmpty()){
            throw new IllegalArgumentException("Eingabe darf nicht leer sein!!");
        } else {
            return value;
        }
    }

    private String checkMaxLength(String value, int maxLength){
        if (value.length() > maxLength){
            throw new IllegalArgumentException("Die Beschreibung darf nicht länger als 15 Zeichen haben!!");
        } else {
            return value;
        }
    }

    private int checkInteger(String value){
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Der eingebene Wert representiert keine Zahl!!");
        }
    }

    private double checkDouble(String value){
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Der eingebene Wert kann keine Dezimalstelle haben!!");
        }
    }

    public void loadArticle(int id){
        Article article = dao.read(id);
        System.out.println(id);
        txtarticlenum.setText(article.getId() + "");
        txtarticlename.setText(article.getDescription() + "");
        txtarticleprice.setText(article.getPrice() + "");
        txtarticleamount.setText(article.getStock() + "");
    }

}