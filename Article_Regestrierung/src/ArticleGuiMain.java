import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ArticleGuiMain {

    public static void main(String[] args) throws IOException {
        new Articlemanagement();
        Connection connection = null;
        try {
            Class.forName("org.mariadb.jdbc.Driver");

            connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/artikelverwaltung","root","");
            System.out.println("Connected to database successfully");

            Statement statement = connection.createStatement();
            String sql = "select * from artikel";
            ResultSet resultSet = statement.executeQuery(sql);

            ArrayList<Article> articleDatabases = new ArrayList<>();
            while(resultSet.next()){
                articleDatabases.add(new Article(resultSet.getInt(1),resultSet.getString(2),resultSet.getInt(3),resultSet.getInt(4)));
            }

            for (Article arc: articleDatabases) {
                System.out.println(arc.toString());
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
                System.out.println("Connection closed");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
