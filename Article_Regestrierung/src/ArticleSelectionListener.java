import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ArticleSelectionListener implements ListSelectionListener {
        private JTable table;
        private ArticleGUI articleGUI;

        public ArticleSelectionListener (JTable table, ArticleGUI articleGUI) {
            this.table = table;
            this.articleGUI = articleGUI;
        }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()){
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        int selectedRow = lsm.getMinSelectionIndex();

        if( selectedRow >= 0 ){
            int id = (int) table.getValueAt(selectedRow,0);
            articleGUI.loadArticle(id);
        }
    }
}

