import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleTableModel extends AbstractTableModel {

    private final String[] columNames = {"ID","Description","Price","Stock"};

    private List<Article> articles;

    public void initData(List<Article> articles) {
        this.articles = articles;
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        if (articles != null) {
            return articles.size();
        }
        return 0;
    }

    @Override
    public int getColumnCount() {
        return columNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Article article = articles.get(rowIndex);
        switch (columnIndex){
            case 0:
                return article.getId();
            case 1:
                return article.getDescription();
            case 2:
                return article.getPrice();
            case 3:
                return article.getStock();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return columNames[column];
    }
}