import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.channels.SelectionKey;
import java.util.List;

public class Articlelist extends JPanel {


    public Articlelist(ArticleTableModel tableModel, ArticleGUI articleGUI, ArticleDataAccessObject dao) {
        setPreferredSize(new Dimension(450, 250));
        setLayout(new FlowLayout());

        JTable jTable = new JTable(tableModel);

        jTable.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        ListSelectionModel rowSelectionModel = jTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ArticleSelectionListener(jTable,articleGUI));


        JScrollPane scrollPane = new JScrollPane(jTable);

        scrollPane.setPreferredSize(new Dimension(400, 150));
        scrollPane.setVerticalScrollBarPolicy(scrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(400, 75));
        panel.setLayout(null);

        JButton showArticle = new JButton("zeige Artikelliste");
        showArticle.setBounds(0, 5, 125, 25);
        showArticle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.initData(dao.readAllMessages());
            }
        });
        JButton exit = new JButton("beenden");
        exit.setBounds(275, 5, 125, 25);
        exit.addActionListener(e -> System.exit(0));
        panel.add(showArticle);
        panel.add(exit);
        add(panel);
    }
}