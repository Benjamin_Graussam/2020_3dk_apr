import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

    public class Articlemanagement extends JFrame {
        String host;
        String port;
        String database;
        String user;
        String password;

    public Articlemanagement() throws IOException {
        super("Artikelverwaltung");
        setSize(450,510);
        setLayout(new BorderLayout());

        Properties properties = new Properties();
        FileInputStream at = new FileInputStream("ArticlePropertie");
        properties.load(at);
        at.close();
        host = (String) properties.get("host");
        port = (String) properties.get("port");
        database = (String) properties.get("database");
        user = (String) properties.get("user");
        password = (String) properties.get("password");


        setDefaultCloseOperation(EXIT_ON_CLOSE);

        ArticleDataAccessObject dao = new ArticleDataAccessObject(host,port,database,user,password);

        ArticleTableModel tableModel = new ArticleTableModel();
        tableModel.initData(dao.readAllMessages());
        ArticleGUI gui = new ArticleGUI(dao,tableModel);
        add(gui,BorderLayout.NORTH);
        add(new Articlelist(tableModel,gui,dao),BorderLayout.SOUTH);

        setVisible(true);
    }

}