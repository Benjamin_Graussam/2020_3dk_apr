import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;


public class Account extends JPanel
{
    private JLabel lblScore;
    private JButton btnAddStake;

    public Account()
    {
        setLayout(new FlowLayout(FlowLayout.CENTER,10,20));
        setBorder(new EmptyBorder(6,10,10,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblAccount = new JLabel("Konto");
        lblAccount.setHorizontalAlignment(SwingConstants.CENTER);
        lblAccount.setPreferredSize(new Dimension(120,30));
        lblAccount.setOpaque(true);
        lblAccount.setBackground(Color.WHITE);
        add(lblAccount);

        lblScore = new JLabel();
        lblScore.setText("0");
        lblScore.setHorizontalAlignment(SwingConstants.CENTER);
        lblScore.setPreferredSize(new Dimension(30,30));
        lblScore.setOpaque(true);
        lblScore.setBackground(Color.WHITE);
        add(lblScore);

        btnAddStake = new JButton("Einsatz zahlen");
        btnAddStake.setPreferredSize(new Dimension(120,30));
        add(btnAddStake);
    }

    // FÜr den Aufruf in der Game Klasse
    public JButton stake_add() { return btnAddStake; } // CamelCase in Methoden Namen verwenden: stakeAdd statt stake_add

    public int actuelscore(int GOALS) { return Integer.parseInt(lblScore.getText()); } // wozu der GOALS Parameter?? - abgesehen davon: Parameter Namen klein

    public void stake_set(int active) { // CamelCase: stakeSet
        if (active==0) { btnAddStake.setEnabled(false);
        } else { btnAddStake.setEnabled(true); } }

    public void actuelscore2(int points) { lblScore.setText(points+""); }}

