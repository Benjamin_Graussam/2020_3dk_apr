public class Game {
    private int GOALS; // nur Konstanten in Großbuchstaben

    public Game (Setnumber number1, ThrowDice DICE, Account acc) {
        GOALS = 0;

        acc.stake_add().addActionListener(e -> {

            if (acc.actuelscore(GOALS) == 0){
                GOALS+=5;
                acc.actuelscore(GOALS); // hier sollte wohl actuelscore2(...) verwendet werden ?!?
                DICE.setBtnDice(1);
                acc.stake_set(0);
            } });
        DICE.getBtnDice().addActionListener(e -> {

            if (acc.actuelscore(GOALS) > 0){
                DICE.DICE_ran();
                int[] numbers = new int[]{DICE.Dice1(),DICE.Dice1(),DICE.Dice3()};
                int pastscore = GOALS;

                if (number1.Text_number() <= 6 && number1.Text_number() >= 1 ){
                    if (number1.Text_number() == numbers[0] ||  number1.Text_number() == numbers[1] ||  number1.Text_number() == numbers[2]) {
                        for (int num:numbers) {
                            if (number1.Text_number() == num){
                                GOALS=+1;
                                acc.actuelscore2(GOALS); }}

                    }else if (pastscore==GOALS){
                        GOALS=-1;
                        acc.actuelscore2(GOALS);
                    }
                }else {
                    System.out.println("Es ist ein Fehler aufgetreten");
                }
            }else {
                DICE.setBtnDice(0);
                acc.stake_set(1);
            } }); }
}
