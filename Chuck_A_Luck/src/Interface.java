import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame
{
    private final String[] ARR = new String[]{"lblDice1", "lblDice2","lblDice3"};

    private JPanel heading = new JPanel();
    private JLabel lblHeading = new JLabel("CHUCK A LUCK");

    private Account myAccount = new Account();
    private Setnumber myNumber = new Setnumber();
    private ThrowDice myDice = new ThrowDice();

    public Interface(String title)
    {
        super(title);
        add(myAccount, BorderLayout.WEST);
        add(myNumber, BorderLayout.CENTER);
        add(myDice, BorderLayout.EAST);

        new Game(myNumber,myDice, myAccount);
        add(heading, BorderLayout.NORTH);
        lblHeading.setFont(new Font("Arial", Font.BOLD, 18));
        heading.setBackground(Color.BLUE);
        heading.add(lblHeading);
        lblHeading.setHorizontalAlignment(SwingConstants.CENTER);
        lblHeading.setForeground(Color.WHITE);
    }

}