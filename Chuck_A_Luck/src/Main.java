import javax.swing.*;

public class Main
{
    public static void main(String[] args)
    {
        Interface myInterface = new Interface("chuck a luck");
        myInterface.setSize(480,250);
        myInterface.setLocationRelativeTo(null);
        myInterface.setResizable(false);
        myInterface.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        myInterface.setVisible(true);
    }
}

// 9 Punkte
// Würfeln-Button zu Beginn aktiv: -1
// Würfeln-Ergebnisse werden nicht angezeigt: -2
// Insgesamt ist dein Code sehr chaotisch, schwer zu lesen und zu verstehen

