import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Random;

public class ThrowDice extends JPanel
{
    private JButton btnDice;
    private JLabel lblDice1;
    private JLabel lblDice2;
    private JLabel lblDice3;

    public ThrowDice()
    {
        setLayout(new FlowLayout(FlowLayout.CENTER,10,10));

        setBorder(new EmptyBorder(16,10,15,10));
        setPreferredSize(new Dimension(160, 0));
        setBackground(Color.LIGHT_GRAY);

        JLabel lblDice = new JLabel("Würfeln");
        lblDice.setPreferredSize(new Dimension(120,30));
        lblDice.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice.setOpaque(true);
        lblDice.setBackground(Color.WHITE);
        add(lblDice);

        JPanel dicePanel = new JPanel();
        dicePanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
        dicePanel.setOpaque(false);
        lblDice1 = new JLabel("");
        lblDice1.setPreferredSize(new Dimension(30,30));
        lblDice1.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice1.setOpaque(true);
        lblDice1.setBackground(Color.WHITE);
        dicePanel.add(lblDice1);

        lblDice2 = new JLabel("");
        lblDice2.setPreferredSize(new Dimension(30,30));
        lblDice2.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice2.setOpaque(true);
        lblDice2.setBackground(Color.WHITE);
        dicePanel.add(lblDice2);

        lblDice3 = new JLabel("");
        lblDice3.setPreferredSize(new Dimension(30,30));
        lblDice3.setHorizontalAlignment(SwingConstants.CENTER);
        lblDice3.setOpaque(true);
        lblDice3.setBackground(Color.WHITE);
        dicePanel.add(lblDice3);

        add(dicePanel);

        btnDice = new JButton("Würfeln");
        btnDice.setPreferredSize(new Dimension(120,30));
        add(btnDice);
    }

    public void DICE_ran(){ // Methode passt so, allerdings wird sie nicht korrekt verwendet
        Random rand = new Random();
        lblDice1.setText((rand.nextInt(6)+1)+"");
        lblDice2.setText((rand.nextInt(6)+1)+"");
        lblDice3.setText((rand.nextInt(6)+1)+"");
    }

    public JButton getBtnDice() {
        return btnDice;
    }

    public void setBtnDice(int active) {
        if (active == 0) {
            btnDice.setEnabled(false);
        } else {
            btnDice.setEnabled(true);
        }
    }

    public int Dice1() {
        return Integer.parseInt(lblDice1.getText());
    }

    public int Dice2() {
        return Integer.parseInt(lblDice2.getText());
    }

    public int Dice3() { return Integer.parseInt(lblDice3.getText());
    }
}



