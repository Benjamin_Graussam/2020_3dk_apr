import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ConsoleOutputActionListener extends MouseAdapter {
    int yourclicks = 0;

    @Override
    public void mouseClicked(MouseEvent e) {

        this.yourclicks += 1;
    }

    public int getYourclicks(){
        return this.yourclicks;
    }
}