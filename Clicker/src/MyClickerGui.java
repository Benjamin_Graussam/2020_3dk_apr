import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public class MyClickerGui extends JFrame  {

    public MyClickerGui (String title) {
        super(title);
        setVisible(true);
        setLayout(new FlowLayout());
        ConsoleOutputActionListener click = new ConsoleOutputActionListener();
        addMouseListener(click);
        setSize(500,500);

        JButton clickerCount = new JButton("Button");
        add(clickerCount);
        JLabel mylabel = new JLabel();
        add(mylabel);
        clickerCount.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int var = click.getYourclicks  ();
                mylabel.setText(String.valueOf(var));

            }
        });
    }
}

