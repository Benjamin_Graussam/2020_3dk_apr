import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseAccess {

    private String user;

    private String password;

    private String connectionURL;

    public DatabaseAccess (String host, String port, String dbName, String user, String password, String dbVendor) {
        this.user = user;
        this.password = password;
        if ("mariadb".equals(dbVendor)) {
            initMariadb(host, port, dbName);
        } else if ("oracle".equals(dbVendor)) {
            initOracle(host, port, dbName);
        }
    }

    private void initMariadb (String host, String port, String dbName) {
        this.connectionURL = "jdbc:mariadb://" + host + ":" + port + "/" + dbName;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }

    private void initOracle (String host, String port, String dbName) {
        this.connectionURL = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }
}