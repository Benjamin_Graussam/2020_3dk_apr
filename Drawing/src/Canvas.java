import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

public class Canvas extends JPanel {


    public Canvas() {

      setBackground(Color.BLACK);
      setForeground(Color.MAGENTA);

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(10));
        g2.draw(new Line2D.Float(30, 20, 80, 90));
        g.drawString("Hello Word",20,50);

    }
}
