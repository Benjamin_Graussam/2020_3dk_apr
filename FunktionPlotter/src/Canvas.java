import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
//Benjamin
public class Canvas extends JPanel {
    public Canvas() {
        setBackground(Color.black);
        setForeground(Color.red);
        setPreferredSize(new Dimension(500, 500));

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawLine(250,0,250,500);
        g.drawLine(0,250,500,250);
        g.drawLine(220,245,220,255);
        g.drawLine(280,245,280,255);
        g.drawLine(245,220,255,220);
        g.drawLine(245,280,255,280);
        g.drawString("+1",260,220);
        g.drawString("-1",210,240);
        g.drawString("-1", 255,295);
        g.drawString("+1", 285,265);

        g.setColor(Color.YELLOW);
        for (double i = -10; 1 < 10; i+=0.005){
            double y = Math.tan(i   );
            int xPos = (int) (i * 50);
            int yPos = (int) (-y*50);
            g.drawOval(xPos,yPos,1,1);



        }
    }
}
