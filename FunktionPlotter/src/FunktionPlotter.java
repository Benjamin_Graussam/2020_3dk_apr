import javax.swing.*;
import java.awt.*;
//Benjamin
public class FunktionPlotter extends JFrame {
    public FunktionPlotter()   {

        setLayout(new FlowLayout());

        Canvas drawingArea = new Canvas();
        add(drawingArea);

        JButton tanButton = new JButton("tan(x)");
        add(tanButton);

        JButton powerButton = new JButton("x^2");
        add(powerButton);

        setVisible(true);
        setSize(700,550);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
