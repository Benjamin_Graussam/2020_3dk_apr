import java.sql.*;
import java.util.HashMap;

public class ArticleDataAccessObject {
    private final String user;
    private final String password;
    private final String databaseurl;
    public HashMap<Integer, League> database;

    public ArticleDataAccessObject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.database = new HashMap<>();
        this.databaseurl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }


    public Championsship loadForLeague(String leagueName) {
        Connection connection = connect();
        String sql = "select c.id, l.id, l.name, l.year, t.id, t.name\n" +
                "from championship c\n" +
                "join liga l on c.league = l.id\n" +
                "join team t on c.team = t.id\n" +
                "where l.name = '" + leagueName + "';";

        Statement select = null;
        //Championship.addTeam(t.id, t.name);
        try {
            select = connection.createStatement();
            ResultSet resultSet = select.executeQuery(sql);
            Championsship championship = null;
            if (resultSet.next()) {
                League league = new League(resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4));
                championship = new Championsship(resultSet.getInt(1), league);
                championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
            }
            while (resultSet.next()) {
                championship.addTeam(new Team(resultSet.getInt(5), resultSet.getString(6)));
            }
            return championship;
        } catch (SQLException throwables) {
            System.err.println("Could not run query!");
            throwables.printStackTrace();
        } finally {
            close(connection);
        }
        return null;
    }

    private Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseurl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
