import java.util.ArrayList;
import java.util.List;

    public class  Championsship {
        private int id;
        private League league;
        private List<Team> team;
        public Championsship(int id, League league) {
            this.id = id;
            this.league = league;
            this.team = new ArrayList<>();
        }
        public void addTeam(Team t){
            this.team.add(t);
        }
        public int getId() {
            return id;
        }
        public League getLeague() {
            return league;
        }
        public List<Team> getTeam() {
            return team;
        }
        @Override
        public String toString() {
            return "Championship{" +
                    "id=" + id +
                    ", league=" + league +
                    ", team=" + team +
                    '}';
        }
    }
