import java.time.LocalDate;
import java.util.ArrayList;

public class FussballmeisterschaftMain {
    public static void main(String[] args) {

        Team heidenreichstein = new Team(1, "Heidenreichstein");
        Team gastern = new Team(2, "Gastern");
        Team vitis = new Team(3, "Vitis");

        League waldviertel = new League(1, "1. Klasse Waldviertel", "2021/2022");

        /*ArrayList<Team> teams = new ArrayList<>();
        teams.add(heidenreichstein);
        teams.add(gastern);
        teams.add(vitis);*/

        Championsship championship = new Championsship(1, waldviertel);
        championship.addTeam(heidenreichstein);
        championship.addTeam(gastern);
        championship.addTeam(vitis);

        ArticleDataAccessObject dao = new ArticleDataAccessObject("localhost", "3306", "fussballmeisterschaft", "root", "");
        Championsship meisterschaft = dao.loadForLeague("1. Klasse Waldviertel");


        /*DataAccess dataAccess = new DataAccess("localhost", "3306", "fussballmeisterschaft");
        FMdao fMdao = new FMdao(dataAccess);
        Championship meisterschaft = fMdao.loadForLeague("1. Klasse Waldviertel");*/


        gameDataAccessobject gameDataAccessObject = new gameDataAccessobject("localhost", "3306", "fussballmeisterschaft", "root", "");



        vereine(meisterschaft);
    }

    public static void vereine(Championsship championship) {
        String headline = championship.getLeague().getName() + " (" + championship.getLeague().getYear() + ")";
        System.out.println(headline);
        for (int i = 0; i < championship.getLeague().getName().length(); i++) {
            System.out.print("-");
        }
        System.out.println("");

        for (Team t : championship.getTeam()) {
            System.out.println(" " + t.getName());
        }

    }

}