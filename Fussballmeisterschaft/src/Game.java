public class Game {

    private int id;
    private Team homeTeam;
    private Team awayTeam;
    private int scoreHome;
    private int scoreAway;
    private String date;

    public Game(int id, Team homeTeam, Team awayTeam, int scoreHome, int scoreAway, String date) {
        this.id = id;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.scoreHome = scoreHome;
        this.scoreAway = scoreAway;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public int getScoreHome() {
        return scoreHome;
    }

    public int getScoreAway() {
        return scoreAway;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Game:");
        sb.append("id = ").append(id);
        sb.append("homeTeam = ").append(homeTeam);
        sb.append("awayTeam = ").append(awayTeam);
        sb.append("scoreHome = ").append(scoreHome);
        sb.append("scoreAway = ").append(scoreAway);
        sb.append("date = ").append(date);
        return sb.toString();
    }
}