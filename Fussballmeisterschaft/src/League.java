public class League {
    private int id;
    private String name;
    private String year;
    public League(int id, String name, String year) {
        this.id = id;
        this.name = name;
        this.year = year;
    }
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public String getYear() {
        return year;
    }
}
