import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class gameDataAccessobject {

    private final String user;
    private final String password;
    private final String databaseurl;
    public HashMap<Integer, League> database;


    public gameDataAccessobject(String host, String port, String database, String user, String password) {
        this.user = user;
        this.password = password;
        this.database = new HashMap<>();
        this.databaseurl = "jdbc:mariadb://" + host + ":" + port + "/" + database;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load database driver", e);
        }
    }
    public void insert(ArrayList<Game> games) {
        Connection connection = connect();
        Statement insert = null;
        for (int i = 0; i < games.size(); i++) {
            String sql = "insert into game(home_team, away_team, score_home, score_away, game_date) VALUES ('"+ games.get(i).getHomeTeam() + "' , '" + games.get(i).getAwayTeam() + "','" + games.get(i).getScoreHome() + "' , '" + games.get(i).getScoreAway() + "' , '" + games.get(i).getDate() + "')";
            insert = null;
            try {
                insert = connection.createStatement();
                insert.execute(sql);
            } catch (SQLException throwables) {
                throw new RuntimeException("Could not create statement", throwables);
            }
        }
        close(connection);
    }
    private Connection connect() {
        try {
            return DriverManager.getConnection(this.databaseurl, this.user, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }
    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}

