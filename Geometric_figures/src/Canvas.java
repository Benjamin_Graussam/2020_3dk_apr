import javax.swing.*;
import java.awt.*;

public class Canvas extends JPanel {

    private int xPos;
    private int yPos;

    private GeometricFiguresGUI geometricFiguresGUI;

    public Canvas(GeometricFiguresGUI geometricFiguresGUI) {
        setBackground(Color.BLACK);
        setPreferredSize(new Dimension(500,500));
        this.geometricFiguresGUI = geometricFiguresGUI;

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setForeground(Color.YELLOW);
        if("Rechteck".equals(geometricFiguresGUI.getMode())){
            g.drawRect(xPos,yPos,100,80);
        }else if ("Kreis".equals(geometricFiguresGUI.getMode())){
            g.drawOval(xPos,yPos,100,100);
        }else if ("Scheibe".equals(geometricFiguresGUI.getMode())){
            g.fillOval(xPos,yPos,100,100);
        }


    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
}



