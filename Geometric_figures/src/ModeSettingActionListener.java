import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModeSettingActionListener implements ActionListener {

    private final GeometricFiguresGUI geometricFiguresGUI;
    public ModeSettingActionListener(GeometricFiguresGUI geometricFiguresGUI) {
        this.geometricFiguresGUI = geometricFiguresGUI;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        geometricFiguresGUI.setMode(e.getActionCommand());
    }
}
