class AustianPerson extends Person {

    public AustianPerson(String name) {
        super(name, "austrian");

        if (name.equals("Johann")){
            this.name = "Hauns";
        }

    }

    @Override
    public String greet() {
        return "Servas " +name;
    }
}
