public class EnglishPerson extends Person {

    public EnglishPerson(String name) {
        super(name, "english");
    }

    @Override
    public String greet() {
        return "Hello " + this.name + "!";
    }
}
