public class GermanPerson extends Person {

    public GermanPerson(String name) {
        super(name,"german");
    }

    @Override
    public String greet() {
        return "Hallo " + name + "!";
    }
}
