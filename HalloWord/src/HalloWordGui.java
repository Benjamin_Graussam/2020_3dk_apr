import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class HalloWordGui extends JFrame {
    public HalloWordGui(String title, ArrayList<Person> persons) {
        super(title);

        setLayout(new FlowLayout());

        JLabel label = new JLabel("Text");

        for(Person p: persons){
        JButton button = new JButton(p.getMothertongue());
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText(p.greet());
            }
        });
        add(button);
        }
        add(label);



        setVisible(true);
        setSize(200,200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }
}
