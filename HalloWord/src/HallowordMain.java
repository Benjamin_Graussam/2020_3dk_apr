import javax.naming.Name;
import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

public class HallowordMain {

        public static void main(String[] args) throws IOException {
            handleProgramArguments(args);
            handleProperties();
            handlePropertiesFile();
            handleEnvironmentVariables();

        /*EnglishPerson englishPerson = new EnglishPerson("Johann");

        SpanishPerson spanishPerson = new SpanishPerson("Johann");

        GermanPerson germanPerson = new GermanPerson("Johann");

        FrenchPerson frenchPerson = new FrenchPerson("Johann");

        AustrianPerson austrianPerson = new AustrianPerson("Johann");

        ArrayList<Person> persons = new ArrayList<>();
        persons.add(englishPerson);
        persons.add(spanishPerson);
        persons.add(germanPerson);
        persons.add(frenchPerson);
        persons.add(austrianPerson);

        new HelloWorldGui("Hello World", persons);*/
        }

        private static void handleEnvironmentVariables() {
            System.out.println("Read specific environment variable");
            System.out.println("KONFIG: " + System.getenv("KONFIG"));

            Map<String, String> systemEnv = System.getenv();
            for (String s : systemEnv.keySet()) {
                System.out.println(s + ":" + systemEnv.get(s));
            }
        }

        private static void handlePropertiesFile() throws IOException {
            Properties properties = new Properties();
            FileInputStream in = new FileInputStream("configProperties");
            properties.load(in);
            in.close();
            System.out.println("host=" + properties.getProperty("host"));
            System.out.println("port=" + properties.getProperty("port"));
        }

        private static void handleProperties() {
            String host = System.getProperty("host");
            System.out.println("host=" + host);
            String port = System.getProperty("port");
            System.out.println("port=" + port);
        }

        private static void handleProgramArguments(String[] args) {
            System.out.println("Hello!");
            for (String s : args) {
                System.out.println(s);
            }

            if (args.length == 0) {
                System.err.println("Mindestens 1 argument muss Ã¼bergeben werden");
                System.exit(1);
            }
            System.out.println(args[0]);
        }
    }