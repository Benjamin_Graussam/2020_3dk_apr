public abstract class Person {

    protected String name;

    private String mothertongue;

    public Person(String name,String mothertongue) {
        if (name.equals("Johann")) {
            this.name = "Hansi";
        } else {
            this.name = name;
        }
        this.mothertongue = mothertongue;
    }

    public abstract String greet();

    public String getMothertongue() {
        return mothertongue;
    }
}