public class SpanishPerson extends Person {

    public SpanishPerson(String name) {
        super(name,"spanish");
    }

    @Override
    public String greet() {
        return "Hola " + this.name;
    }
}