import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class HelloDatabaseMain {

    public static void main(String[] args){
        String dbVendor = null;

        if (args.length == 1 && args[0].equals("oracle")){
            dbVendor = "oracle";
        }
        else{
            dbVendor = "mariadb";
        }

        MessageDataAccessObject messageDAO = new MessageDataAccessObject("localhost","3306","hello_word","root","",dbVendor);

        System.out.println(messageDAO.read(2));

        Message message = new Message(2,"test ","test1");

        System.out.println(messageDAO.read(2));
    }
}