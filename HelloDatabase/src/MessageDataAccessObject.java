import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageDataAccessObject {

    private DatabaseAccess databaseAccess;

    public MessageDataAccessObject(String host, String port, String database, String user, String password, String dbVender) {
        databaseAccess = new DatabaseAccess(host, port, database, user, password, dbVender);
    }

    private Connection connect(){
        return databaseAccess.connect();
    }

    private void close (Connection connection) {
        databaseAccess.close(connection);
    }

    public void insert(String text, String language) {
        Connection connection = connect();
        String sql = "insert into message (test,language) VALUES (?, ?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, text);
            preparedStatement.setString(2, language);

            ResultSet resultSet = preparedStatement.executeQuery();
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not insert statement", throwables);
        } finally {
            close(connection);
        }
        System.out.println("Insert Execute successfully");
    }

    public void delete(int id){
        Connection connection = connect();
        String sql = "DELETE FROM message WHERE id = ?";

        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
            System.out.println("Closed Succesfully");
        }
    }

    public void update(Message message){
        Connection connection = connect();
        String sql = "update message set text = ?, language = ? Where id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, message.getMessage());
            preparedStatement.setString(2, message.getLanguage());
            preparedStatement.setInt(3, message.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

        } catch(SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        } finally {
            close(connection);
        }
    }

    public List<Message> readAllMessages() {
        Connection connection = connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = "select * from message";
            ResultSet resultSet;
            resultSet = statement.executeQuery(sql);
            ArrayList<Message> messages = new ArrayList<>();
            while (resultSet.next()) {
                messages.add(new Message(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
            }
            close(connection);
            return messages;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not create statement", throwables);
        }
    }

    public Message read(int id){

        Message m = null;

        Connection connection = connect();
        System.out.println("Insert successfully");
        String sql = "select * from message where id = ?";
        Statement statement = null;
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                m = new Message(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3));
            }
        } catch(SQLException throwables){
            throw new RuntimeException("Could not create statment", throwables);
        } finally {
            close(connection);
            System.out.println("Closed");
        }

        return m;
    }
}
