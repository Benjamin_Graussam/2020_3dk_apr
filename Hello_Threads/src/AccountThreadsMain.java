public class AccountThreadsMain {
    public static void main(String[] args) {
            Account account = new Account(50);

            Thread alice = new PersonThread(account, "Alice");
            Thread bob = new PersonThread(account, "Bob");
            alice.start();
            bob.start();

        }

    }