public class MyThread extends Thread {

    private String name;
    private int sleepTime; // in milliseconds

    public MyThread(String name, int sleepTime) {
        this.name = name;
        this.sleepTime = sleepTime;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(this.sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Mein Name ist " + name);
    }
}
