import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLOutput;

public class LoginGui extends JFrame {
    public LoginGui(String title) throws HeadlessException {
        JFrame gui = new JFrame();
        gui.setLayout(null);
        gui.setSize(400,300);

        UserDataAccessObject userdao = new UserDataAccessObject("localhost","3306","login_gui_db","root","");

        JPanel panel = new JPanel();
        gui.add(panel);

        JLabel label1 = new JLabel("User");
        label1.setBounds(10,10,80,25);
        gui.add(label1);

        JLabel label2 = new JLabel("Password");
        label2.setBounds(10,40,160,25);
        gui.add(label2);

        JTextField userTextfield = new JTextField();
        userTextfield.setBounds(140,10,120,20);
        gui.add(userTextfield);

        JPasswordField passwortTextfield = new JPasswordField();
        passwortTextfield.setBounds(140,40,120,20);
        gui.add(passwortTextfield);

        JButton loginbutoon = new JButton("Login");
        loginbutoon.setBounds(10,80,100,25);
        gui.add(loginbutoon);
        loginbutoon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Login       " + userdao.login(userTextfield.getText(), new String(passwortTextfield.getPassword())));
                System.out.println("secure-Login " + userdao.secruteLogin(userTextfield.getText(), new String(passwortTextfield.getPassword())));
            }
        });

        JButton registerbutton = new JButton("Registieren");
        registerbutton.setBounds(50,120,135,23);
        gui.add(registerbutton);

        JButton passwortVergessen = new JButton("Passwort vergessen");
        passwortVergessen.setBounds(190,120,165,23);
        gui.add(passwortVergessen);

        gui.setVisible(true);
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}




