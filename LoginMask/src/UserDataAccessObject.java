import java.sql.*;

public class UserDataAccessObject {

    private final String connectionUrl;
    private final String username;
    private final String password;

    public UserDataAccessObject(String host, String port, String databaseName, String username, String password) {
        this.connectionUrl = "jdbc:mariadb://" + host + ":" + port + "/" + databaseName;
        this.username = username;
        this.password = password;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not load databse driver", e);
        }
    }

    public boolean login(String userName, String password) {
        Connection connection = connect();
        String sql = "SELECT * FROM user WHERE username = '" + userName + "' AND password = '" + password + "'";
        Statement login = null;
        try {
            login = connection.createStatement();
            ResultSet resultSet = login.executeQuery(sql);

            if (resultSet.first()) {
                return true;
            }
            return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run login query", throwables);
        } finally {
            close(connection);
        }
    }

    public boolean secruteLogin(String username, String password) {
        Connection connection = connect();
        String sql = "select * from user where username = ? and password = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                return true;
            }
                return false;
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not run prepareStatement");
        } finally {
            close(connection);
        }
    }


    private Connection connect() {
        try {
            return DriverManager.getConnection(this.connectionUrl, this.username, this.password);
        } catch (SQLException throwables) {
            throw new RuntimeException("Could not connect to database", throwables);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.out.println("Closed connection");
        }
    }
}

