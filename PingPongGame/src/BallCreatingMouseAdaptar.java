import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BallCreatingMouseAdaptar extends MouseAdapter {

private PingPongPanel pingPongPanel;
private final PingPongBar bar;

    public BallCreatingMouseAdaptar(PingPongPanel pingPongPanel,PingPongBar bar) {
        this.pingPongPanel = pingPongPanel;
        this.bar = bar;
    }

    @Override
    public void mousePressed(MouseEvent e) {

        PingPongBall ball = new PingPongBall(pingPongPanel,e.getX(),e.getY(),this.bar);
        ball.start();

}
}
