import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BarMovingKeyAdapter extends KeyAdapter {
    private final PingPongBar bar;

    private static final int KEY_CODER_RIGHT_ARROW = 39;
    private static final int KEY_CODER_LEFT_ARROW = 37;


    public BarMovingKeyAdapter(PingPongBar bar) {
        this.bar = bar;
    }

    @Override
    public void keyPressed(KeyEvent e) {
//        if (e.getKeyCode() == KEY_CODER_RIGHT_ARROW){
//        bar.moveleft();}
//        else if (e.getKeyCode() == KEY_CODER_LEFT_ARROW){
//        bar.moveright();}
        //   }
        int Keycode = e.getKeyCode();
        switch (Keycode) {
            case KeyEvent.VK_LEFT:
                bar.moveleft();
                break;
            case KeyEvent.VK_RIGHT:
                bar.moveRight();
                break;

        }
    }
}
