import javax.swing.*;
import java.awt.*;

public class PingPongGui extends JFrame {
        public PingPongGui(int numberOfBalls) {
            super("PingPong");

            String ballin;
            ballin = System.getenv("BALLCOUNT");
            System.out.println(ballin);

            PingPongPanel canvas = new PingPongPanel();

            setLocationRelativeTo(null);
            setSize(500,400);
            setContentPane(canvas);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setVisible(true);


            canvas.iniBar();

            for (int i = 0; i < numberOfBalls; i++) {
                PingPongBall ball = new PingPongBall(canvas,i*20+10,i*20+10,canvas.getBar());
                ball.start();

            }
        }
    }
