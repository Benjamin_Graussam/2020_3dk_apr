import java.util.Map;

public class PingPongMain {
    public static void main(String[] args) {

        if (args.length == 0) {
            System.err.println("Parameter 'noOfBalls' missing");
            System.exit(-1);
        }

        String noOfBalls = args[0];

        int n = 0;
        try {
            n = Integer.parseInt(noOfBalls);

        } catch (NumberFormatException e) {
            System.err.println("Parameter 'noOfBalls' must be a number");
            System.exit(-1);
        }
        new PingPongGui(n);
    }
}
