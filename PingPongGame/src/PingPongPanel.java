import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PingPongPanel extends JPanel {


    private PingPongBar bar;

    public PingPongPanel() {


        setBackground(Color.YELLOW);
        setFocusable(true);
    }
    public void iniBar(){
        bar = new PingPongBar(this);
        addKeyListener(new BarMovingKeyAdapter(bar));
        addMouseListener(new BallCreatingMouseAdaptar(this,bar));

    }

    public PingPongBar getBar() {
        return bar;
    }
}
