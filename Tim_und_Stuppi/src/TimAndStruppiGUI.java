
import javax.swing.*;
import java.awt.*;

public class  TimAndStruppiGUI extends JFrame {

    private JButton buttonTim;
    private JButton buttonAnd;
    private JButton buttonStruppi;

    public TimAndStruppiGUI(String title) {
        super(title);


        createButtons();
        addButtonFunctionality();


        setSize(250,100);

        setLayout(new FlowLayout());

        addWindowListener(new WindowClosingListener());

        setVisible(true);
    }

    private void createButtons () {
        buttonTim = new JButton("Tim");
        add(buttonTim);

        buttonAnd = new JButton("and");
        add(buttonAnd);

        buttonStruppi = new JButton("Struppi");
        add(buttonStruppi);
    }

    private void addButtonFunctionality () {
        buttonTim.addActionListener(new ConsoleOutputActionListener());

        buttonAnd.addActionListener(new ConsoleOutputActionListener());

        buttonStruppi.addActionListener(new ConsoleOutputActionListener());
    }
}
