public class ColorSwitchingThread extends Thread{
private  TimeGui timeGui;

    public ColorSwitchingThread(TimeGui timeGui) {
        this.timeGui = timeGui;
    }
    @Override
    public void run() {
        while (true){
            timeGui.switchColor();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
