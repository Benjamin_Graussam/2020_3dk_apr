import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TimeGui extends JFrame {

    private JLabel timeLabel;

    private Color[] colors = {Color.BLACK, Color.BLUE, Color.RED};

    private int currentColorPosition;

    public TimeGui() {

        super("Uhrzeit anzeigen");
        JPanel contentPane = new JPanel();

        JButton changeColorButton = new JButton("Farbwechsel starten");
        ColorSwitchingThread t = new ColorSwitchingThread(this);
        changeColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                t.start();
                switchColor();
            }
        });

        this.timeLabel = new JLabel(getTimeText());

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTime();
            }
        });
        timer.start();

        contentPane.add(changeColorButton);
        contentPane.add(timeLabel);

        setContentPane(contentPane);
        setPreferredSize(new Dimension(300, 80));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }

    public void switchColor() {
        currentColorPosition++;
        if ( currentColorPosition > colors.length-1 ) {
            currentColorPosition = 0;
        }
        timeLabel.setForeground(colors[currentColorPosition]);
    }

    public void setTime() {
        timeLabel.setText(getTimeText());
    }

    public String getTimeText() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return LocalTime.now().format(dateTimeFormatter);
    }
}
